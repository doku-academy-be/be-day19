package com.example.day19.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
@Getter
@Setter
public class UserRequest implements Serializable {

    @JsonProperty(value = "username", required = true)
    private String username;
    @JsonProperty(value = "email", required = true)
    private String email;
    @JsonProperty(value = "password",required = true)
    private String password;

}
