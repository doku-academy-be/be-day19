package com.example.day19.controller;

import com.example.day19.dto.UserRequest;
import com.example.day19.dto.UserResponse;

import com.example.day19.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    UserService userService;
    @PostMapping("/login")
    public ResponseEntity<UserResponse> login(@RequestBody UserRequest req){
        try {
            UserResponse res = userService.Login(req);
            return new ResponseEntity<>(HttpStatus.OK);
        }catch (Exception ex){
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("/register")
    public ResponseEntity<UserResponse> createNewUser(@RequestBody UserRequest req) {
        try{
            UserResponse res = userService.Register(req);
            return new  ResponseEntity<>(res,HttpStatus.CREATED);
        }catch (Exception ex){
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    }



}
