package com.example.day19.service;

import com.example.day19.dto.UserRequest;
import com.example.day19.dto.UserResponse;
import com.example.day19.entities.User;
import com.example.day19.repositories.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.LogManager;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.logging.Logger;

@Slf4j
@Service
public class UserService {
    @Autowired
    ModelMapper modelMapper;

    @Autowired
    UserRepository userRepository;

    private User convertToEntity(UserRequest req) {
        return modelMapper.map(req, User.class);
    }

    private UserResponse convertToDto(User user) {
        return modelMapper.map(user, UserResponse.class);
    }

    @Transactional
    public UserResponse Register(UserRequest req) {

        User user = convertToEntity(req);

        Optional<User> usernameOptional = userRepository.findByUsername(req.getUsername());
        Optional<User> emailOptional = userRepository.findByEmail(req.getEmail());
        if (usernameOptional.isPresent()||emailOptional.isPresent()) {
            log.error("Username or email already exists");
            throw new RuntimeException("Data already exists.");
        }
        user.setUsername(user.getUsername());
        user.setEmail(user.getEmail());
        user.setPassword(user.getPassword());
        User createdUser = userRepository.save(user);
        log.info("{} {} Registered as a user", req.getUsername(), req.getEmail());
        return convertToDto(createdUser);
    }

    public UserResponse Login(UserRequest req){
        User user = convertToEntity(req);
        Optional<User> FindUser = userRepository.findByUsername(req.getUsername());
        if (FindUser.isEmpty()){
            log.error("Data user not found");
            throw new RuntimeException("Data User Not Found");
        }
        if (!FindUser.get().getPassword().equals(req.getPassword())){
            log.error("Wrong password");
            throw new RuntimeException("Wrong Password");
        }
        log.info("{} succesfully login", req.getUsername());
        return convertToDto(user);
    }


}
